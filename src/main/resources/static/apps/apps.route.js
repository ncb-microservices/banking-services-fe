app.config(function($routeProvider) {
	$routeProvider
	.when("/home", {
		templateUrl : "/apps/components/home/home.html",
		controller  : 'homeCtrl'
	})
		
	.when("/customerinformation", {
		templateUrl : "/apps/components/customers/customerinformation.html",
		controller  : 'customerinformationCtrl'
	})
	.when("/creditcardinformation", {
		templateUrl : "/apps/components/creditcards/creditcardinformation.html" ,
		controller  : 'creditcardinformationCtrl'
	})
	.when("/creditcardpayment", {
		templateUrl : "/apps/components/creditcards/creditcardpayment.html" ,
		controller  : 'creditcardpaymentCtrl'
	})
	
	.when("/accountinformation", {
		templateUrl : "/apps/components/accounts/accountinformation.html",
		controller  : 'accountinformationCtrl'
	})
	.when("/fundtransfer", {
		templateUrl : "/apps/components/accounts/fundtransfer.html",
		controller  : 'fundtransferCtrl'
	})
	
	.when("/creditcardtransactions", {
		templateUrl : "/apps/components/creditcards/creditcardtransactions.html",
		controller  : 'creditcardtransactionsCtrl'
	});

			
});
app.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.cache = false;
});

