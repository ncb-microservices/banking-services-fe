app.factory('creditcardservices',  function($http,$window) {
    var ccinformationurlBase ="data/creditcardinformation.txt";
    var cctransactionurlBase ="data/creditcardtransactions.txt";
	var ccpaymentsummaryurlBase ="data/cardpaymentsummary.txt"
	var ccpaymenturlBase = '';
	var dataFactory = {};
    
    dataFactory.getCreditCardInformation = function () {
		return $http.get(ccinformationurlBase);
    };

    dataFactory.getCreditCardTransactions = function () {
		return $http.get(cctransactionurlBase);
    };

	dataFactory.creditcardPaymentSummary = function () {
     	return $http.get(ccpaymentsummaryurlBase);
    };
	
	dataFactory.creditcardPayment = function () {
        return $http.post(ccpaymenturlBase, cust);
    };
	return dataFactory;
});
