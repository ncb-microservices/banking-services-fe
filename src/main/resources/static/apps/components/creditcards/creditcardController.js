app.controller('creditcardinformationCtrl', function ($scope,$http,$window,creditcardservices) {
		$scope.status="";
		$scope.creditcard="";
		$scope.customerID = "";
		getCreditCardInformation();
		
		function getCreditCardInformation() {
			creditcardservices.getCreditCardInformation()
				.then(function (response) {
					$scope.creditcard = response.data[0];
				}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
		}
		
});

app.controller('creditcardtransactionsCtrl', function ($scope,$http,$window,creditcardservices) {
		$scope.status="";
		$scope.cardpayment ="";
		$scope.creditcardTransactions="";
		getCardAccountInformation()
		
		function getCardAccountInformation() {
			creditcardservices.creditcardPaymentSummary()
			.then(function (response) {
				$scope.cardpayment = response.data;
				$scope.selectedCard = $scope.cardpayment[0];
				$scope.getCreditCardTransactions(); 
			}, function (error) {
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
		}
		
		$scope.getCreditCardTransactions = function() {
			creditcardservices.getCreditCardTransactions()
				.then(function (response) {
					$scope.creditcardTransactions = response.data;
				}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
		}
		
		
		$scope.sort = function(keyname){
		   $scope.sortKey = keyname;   //set the sortKey to the param passed
		   $scope.reverse = !$scope.reverse; //if true make it false and vice versa
		}
		


});

app.controller('creditcardpaymentCtrl', function ($scope,$http,$window,creditcardservices,accountservices) {
		$scope.status="";
		$scope.account=[];
		$scope.cardpayment=[];
		getAccountInformation();

		function getAccountInformation() {
			accountservices.getAccountInformation()
				.then(function (response) {
					$scope.account = response.data;
				}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
			
			creditcardservices.creditcardPaymentSummary()
			.then(function (response) {
				$scope.cardpayment = response.data;
			}, function (error) {
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
		}
		
		
});


