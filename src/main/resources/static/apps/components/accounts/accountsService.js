app.factory('accountservices',  function($http,$window) {
    var urlBase = "data/accountinformation.txt";
    var BeneficiaryaccountsurlBase = "data/Beneficiaryaccounts.txt";
	var dataFactory = {};
    var customerID ;
    
	
    dataFactory.getAccountInformation = function () {
		return $http.get(urlBase);
	};
	
	dataFactory.getBeneficiaryAccountsInformation = function () {
		return $http.get(BeneficiaryaccountsurlBase);
	};
	return dataFactory;
});
