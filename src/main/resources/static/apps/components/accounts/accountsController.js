app.controller('accountinformationCtrl', function ($scope,$http,$window,accountservices) {
		$scope.status="";
		$scope.account="";
		$scope.customerID = "";
		getAccountInformation();
		
		function getAccountInformation() {
			accountservices.getAccountInformation()
				.then(function (response) {
					$scope.account = response.data[0];
				}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
		}
		
		
		
		
});

app.controller('fundtransferCtrl', function ($scope,$http,$window,accountservices) {
		$scope.status="";
		$scope.account=[];
		$scope.beneficiaryAccount=[];
		getAccountInformation();
		
		function getAccountInformation() {
			accountservices.getAccountInformation()
				.then(function (response) {
				 $scope.account = response.data;	
				}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
		
			accountservices.getBeneficiaryAccountsInformation()
			.then(function (response) {
				$scope.beneficiaryAccount = response.data;
			}, function (error) {
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
		
		}
       
		$scope.fundTransfer = function() {
			//funtransfer template 
			$scope.fundtransfer = {  
			   "CustomerID":"",
			   "FromAccountNumber":"",
			   "FromAccountBalance":"",
			   "ToAccountNumber":"",
			   "BeneficiaryName":"",
			   "TransferAmount":"",
			   "PaymentPurpose":"",
			   "BankName":"",
			   "IFSCCODE":"",
			   "TransactionFee":""
			}
		   $scope.fundtransfer.FromAccountNumber = $scope.selectedAccount.AccountNumber;
		   $scope.fundtransfer.FromAccountBalance = $scope.selectedAccount.CurrentBalance;
		   $scope.fundtransfer.ToAccountNumber = $scope.beneficiaryToAccount.AccountNumber;
		   $scope.fundtransfer.BeneficiaryName = $scope.beneficiaryToAccount.AccountHolderName;
		   $scope.fundtransfer.BankName = $scope.beneficiaryToAccount.BankName;
		   $scope.fundtransfer.TransferAmount = $scope.transferAmount;
		   $scope.fundtransfer.PaymentPurpose = $scope.purpose;
//		   $window.alert($scope.fundtransfer.TransferAmount  + $scope.fundtransfer.PaymentPurpose +$scope.fundtransfer.ToAccountNumber +$scope.fundtransfer.BeneficiaryName);
		   $window.alert("Fund Transfer");
		   
	   }
		
});




