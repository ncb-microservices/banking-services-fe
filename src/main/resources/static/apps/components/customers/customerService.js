app.factory('customerservices',  function($http,$window) {
    var urlBase = "data/customerinformation.txt";
    var dataFactory = {};
    var customerID ;
    
	dataFactory.getCustomerInformation = function () {
		return $http.get(urlBase);
	};
	return dataFactory;
});
