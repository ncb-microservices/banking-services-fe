app.controller('customerinformationCtrl', function ($scope,$http,$window,customerservices) {
		$scope.status="";
		$scope.customer;
		$scope.customerID = "";
		getCustomerInformation();
		
		function getCustomerInformation() {
			customerservices.getCustomerInformation()
				.then(function (response) {
					$scope.customer = response.data[0];
				}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
		}
		
});

